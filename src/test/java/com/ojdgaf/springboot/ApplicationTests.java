package com.ojdgaf.springboot;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void alwaysPass() {
    }

    @Test
    @Disabled
    void alwaysDisable() {
    }
}
